
---


<center><img src='99_misc/.img/go.png' alt='bash' style='width:500px;'></center>

.footer: Created By Alex M. Schapelle, VAioLabs.io

<div style="page-break-after: always; visibility: hidden"> </div>

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Go programming Language ?
- Who needs it ?
- How Go differs from other languages ?
- Do's and don'ts of Go language.
- Basic concepts of Go programming language.
- Structures, functions and libraries.
- Automation with extended libraries.



### Who Is This course for ?

- Junior/senior system administrators who have no knowledge of Go language.
- - Junior/senior QA engineers who have no knowledge of Go language.
- For junior/senior developers who are still developing on other languages
- For developers who wish to learn build their own tools with Go language.
- Experienced developers seeking to learn new development language.

---

# Course Topics

- [Course Initialization](./00_init/README.md)
- [Introduction to Go](./01_intro/README.md)
- [LAnguage Basics](./02_basics/README.md)
- [Advanced Topics](./03_concepts/README.md)
- [Project Structure](./04_code_base/README.md)
- [Standard Library](./05_std_lib/README.md)
- [Introduction to Viper](./06_intro_to_viper/README.md)
- [Introduction to Cobra](./07_intro_to_cobra/README.md)
- [Working with files](./08_working_with_files/README.md)



---
# About Me
<img src='99_misc/.img/me.jpg' alt='me' style='float:right;width:180px;'>

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

<img src='99_misc/.img/me.jpg' alt='me' style='float:right;width:180px;'>

- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fan
    - Rust fallen
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on the internet in bunch of places:
- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)

<div style="page-break-after: always; visibility: hidden"> </div>


