# Go For DevOps

Course for DevOps and other developers to learn basics of Go Language in addition to emphasis on developing CLI tools

### About The Course Itself ?
We'll learn several topics mainly focused on:
- What is Go programming Language ?
- Who needs it ?
- How Go differs from other languages ?
- Do's and don'ts of Go language.
- Basic concepts of Go programming language.
- Structures, functions and libraries.
- Automation with extended libraries.


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of Go language.
- For junior/senior developers who are still developing on  - For developers who wish to learn build their own tools
- Experienced developers seeking to learn new development language.

### Table Of Content

- [Course Initialization](./00_init/README.md)
- [Introduction to Go](./01_intro/README.md)
- [LAnguage Basics](./02_basics/README.md)
- [Advanced Topics](./03_concepts/README.md)
- [Project Structure](./04_code_base/README.md)
- [Standard Library](./05_std_lib/README.md)
- [Introduction to Viper](./06_intro_to_viper/README.md)
- [Introduction to Cobra](./07_intro_to_cobra/README.md)
- [Working with files](./08_working_with_files/README.md)


