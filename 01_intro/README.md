
---

# Introduction To Go

Course for DevOps and other developers to learn basics of Go Language in addition to emphasis on developing CLI tools

---

# Course overview
In the course we'll going to covers history of Go programming language as well as its basic as follow:

- history
- installation
- language basics
- advanced topics
- project structure
- standard library
- introduction to viper
- introduction to cobra
- working with configuration files


---

# Experience Requirements

Due to a fact that this course is for DevOps, it is suggested for student candidate to have some level of experience with any type of programming language.
Despite that, the course structure enables each and every individual course candidate to dive into the material without prior experience. Those who have experience will gain slow yet strong fundamentals and others  who are not experienced with other programming languages, will gain easy step in.

---

# Tools and system requirements

An individual candidate must have deep level of understanding how his/her OS works, whether it will be Windows, Mac or Linux.

We will suggest to use Debian basic Linux distribution, yet in cases of setup and Go language issues, we'll provide support for the students.

The tools required for the course are:
- Windows, Mac or Linux: Debian Linux distribution is preferred.
- Go compiler
- Text editor or IDE: which ever you like. In case it does not apply to you, please install [vscode](code.visualstudio.com). We'll be using VS-Code as well.
  - Git source control and Github/Gitlab account: please install git on your OS and create github/gitlab account to save course material in there.


---

# Course Goals

- Learn and understand the Go programming language
- Become familiar with the tools and ecosystem
- Have the skills necessary to write useful command line tools


---

# What is go 


---

# Why go for devops


---

# Resource and docs

- The go home page: golang.org
- Package documentation: golang.org/pkg
- Additional documentation: golang.org/doc

---

# Tools Setup


---

# Go compiler install


---
# Text Editor install


---

# Git install


---

# Gitlab Account Setup

---