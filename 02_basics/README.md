
---

# Language Basics


---

# Language Basics

- Hello world
- Learn about basic data types
- Control structures
- Loops
- Error handling

---

# Hello world


- create `hello.go`

```go
package main

import "fmt"

func main(){ // entrypoint
    fmt.println("Hello World") // only use " "
}
// Mazal Tov on your first golang code
```

```sh
# you can use run command to execute the code
go run hello.go

# or you can generate binary as you go
go build hello.go

```

---
# Data types


---

# Learn about basic data types

- Dynamic assignment with `:=`
  - Static assignment is also possible
- Booleans
- Int